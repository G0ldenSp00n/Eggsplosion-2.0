package me.g0ldensp00n.handlers;

import me.g0ldensp00n.main.EggSplosion;
import net.minecraft.server.v1_10_R1.Explosion;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

public class EggHandler implements Listener {
    private final EggSplosion plugin;

    public EggHandler(EggSplosion plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void eggHit(ProjectileHitEvent event) {
        if (event.getEntity() instanceof Egg) {
            Egg egg = (Egg) event.getEntity();
            if (egg.getShooter() instanceof Player) {
                Player player = (Player) event.getEntity().getShooter();
                if (egg.getName().equals(Material.DIAMOND_HOE.name())) {
                    egg.getWorld().createExplosion(egg.getLocation(), 5);
                } else if (egg.getName().equals(Material.GOLD_HOE.name())){
                    egg.getWorld().createExplosion(egg.getLocation(), 6);
                }else if (egg.getName().equals(Material.IRON_HOE.name())) {
                    egg.getWorld().createExplosion(egg.getLocation(), 7);
                } else if (egg.getName().equals(Material.STONE_HOE.name())){
                    egg.getWorld().createExplosion(egg.getLocation(), 8);
                } else {
                    egg.getWorld().createExplosion(egg.getLocation(), 9);
                }
//                TNTPrimed tnt = egg.getWorld().spawn(egg.getLocation(), TNTPrimed.class);
//                Snowball snowball = egg.getWorld().spawn(tnt.getLocation(), Snowball.class);
//                tnt.setCustomName(player.getUniqueId().toString());
//                tnt.setPassenger(snowball);
//                tnt.setFuseTicks(0);
            } else {
                TNTPrimed tnt = egg.getWorld().spawn(egg.getLocation(), TNTPrimed.class);
                tnt.setFuseTicks(0);
            }
        }
    }
}
