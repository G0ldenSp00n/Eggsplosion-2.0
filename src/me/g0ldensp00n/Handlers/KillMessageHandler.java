package me.g0ldensp00n.handlers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Random;

public class KillMessageHandler {

    private Random random = new Random();

    public void killMessages(Player damager, Player player, EntityDamageEvent.DamageCause damageCause) {
        int choice4 = random.nextInt(3) + 1;
        int choice5 = random.nextInt(4) + 1;
        if (damager != player) {
            for (Player playerAll : Bukkit.getOnlinePlayers()) {
                if (damageCause == EntityDamageEvent.DamageCause.FALL) {
                    switch (choice4) {
                        case 1:
                            playerAll.sendMessage(damager.getDisplayName() + " launched " + player.getDisplayName());
                            break;
                        case 2:
                            playerAll.sendMessage(damager.getDisplayName() + " made " + player.getDisplayName() + " discover the force of physics!");
                            break;
                        case 3:
                            playerAll.sendMessage(damager.getDisplayName() + " Hummpty Dumptyed " + player.getDisplayName());
                            break;
                        default:
                            playerAll.sendMessage(damager.getDisplayName() + " launched " + player.getDisplayName());
                            break;
                    }
                } else {
                    switch (choice4) {
                        case 1:
                            playerAll.sendMessage(damager.getDisplayName() + " has egged " + player.getDisplayName());
                            break;
                        case 2:
                            playerAll.sendMessage(damager.getDisplayName() + " yolked " + player.getDisplayName());
                            break;
                        case 3:
                            playerAll.sendMessage(damager.getDisplayName() + " shoved an egg up " + player.getDisplayName() + " butt! OUCH!");
                            break;
                        default:
                            playerAll.sendMessage(damager.getDisplayName() + " has egged " + player.getDisplayName());
                            break;
                    }
                }
            }
        } else {
            for (Player playerAll : Bukkit.getOnlinePlayers()) {
                if (damageCause == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) {
                    switch (choice5) {
                        case 1:
                            playerAll.sendMessage(player.getDisplayName() + " stood too close to the blast!");
                            break;
                        case 2:
                            playerAll.sendMessage(player.getDisplayName() + " doesn't understand how a gun works!");
                            break;
                        case 3:
                            playerAll.sendMessage(player.getDisplayName() + " needs better aim!");
                            break;
                        case 4:
                            playerAll.sendMessage(player.getDisplayName() + " used explosive force!");
                            break;
                        default:
                            playerAll.sendMessage(player.getDisplayName() + " stood too close to the blast!");
                            break;
                    }
                } else if (damageCause == EntityDamageEvent.DamageCause.FALL) {
                    switch (choice5) {
                        case 1:
                            playerAll.sendMessage(player.getDisplayName() + " failed to rocket jump!");
                            break;
                        case 2:
                            playerAll.sendMessage(player.getDisplayName() + " discovered the force of gravity!");
                            break;
                        case 3:
                            playerAll.sendMessage(player.getDisplayName() + " lost there footing!");
                            break;
                        case 4:
                            playerAll.sendMessage(player.getDisplayName() + " fell to death!");
                            break;
                        default:
                            playerAll.sendMessage(player.getDisplayName() + " fell to death by themselves!");
                            break;
                    }
                } else {
                    playerAll.sendMessage(player.getDisplayName() + " died!");
                }
            }
        }
    }
}
