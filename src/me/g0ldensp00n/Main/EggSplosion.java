package me.g0ldensp00n.Main;

import me.g0ldensp00n.GameLogic.CommandHandler.SpawnCommandHandler;
import me.g0ldensp00n.GameLogic.Game;
import me.g0ldensp00n.Handlers.EggHandler;
import me.g0ldensp00n.Handlers.ExplosionRegenHandler;
import me.g0ldensp00n.Listeners.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class EggSplosion extends JavaPlugin{
    public double version = 2.0;
    public String pluginName = "EggSplosion";

    Game mGame;

    ExplosionRegenHandler explosionRegenHandler;


    WoodenHoe woodenHoe;
    StoneHoe stoneHoe;
    IronHoe ironHoe;
    GoldHoe goldHoe;
    DiamondHoe diamondHoe;

    public List<Player> invulnerablePlayers = new ArrayList<>();

    @Override
    public void onEnable(){
        registerHoes();
        registerHandlers();
        mGame = new Game(this);
    }

    private void registerHandlers(){
        new EggHandler(this);
        explosionRegenHandler = new ExplosionRegenHandler(this);
    }

    private void registerHoes(){
        woodenHoe = new WoodenHoe(this);
        stoneHoe = new StoneHoe(this);
        ironHoe = new IronHoe(this);
        goldHoe = new GoldHoe(this);
        diamondHoe = new DiamondHoe(this);
    }

    public boolean invulnerableCheck(Player player){
        if(invulnerablePlayers.contains(player)){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onDisable(){
        explosionRegenHandler.repairAll();
    }
}
