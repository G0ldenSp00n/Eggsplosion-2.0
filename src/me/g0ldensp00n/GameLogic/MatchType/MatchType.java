package me.g0ldensp00n.gamelogic.matchtype;

public enum MatchType {
    LOBBY("Lobby"),
    SOLO("Solo"),
    TEAM("Team");

    private String mDisplayName;

    MatchType(String displayName) {
        mDisplayName = displayName;
    }

    public String getDisplayName() {
        return mDisplayName;
    }
}
