package me.g0ldensp00n.gamelogic.matchtype;

public class Match {
    private MatchType mType;
    private String mDisplayName;

    public Match(MatchType matchType) {
        mType = matchType;
        mDisplayName = matchType.getDisplayName();
    }

    public MatchType getMatchType() {
        return mType;
    }

    public void setMatchType(MatchType matchType) {
        mType = matchType;
        mDisplayName = matchType.getDisplayName();
    }

    public String getDisplayName() {
        return mDisplayName;
    }
}
