package me.g0ldensp00n.gamelogic;

import me.g0ldensp00n.gamelogic.gamemode.ModeType;
import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scoreboard.*;

import java.util.Collection;

public class ScoreboardLogic {
    private EggSplosion mPlugin;
    private ScoreboardManager scoreboardManager;

    //GameBoard SOLO
    private Scoreboard gameBoardSOLO;
    private Objective scoreObjectiveSOLO;

    //GameBoard TEAM
    private  Scoreboard gameBoardTEAM;
    private Objective scoreObjectiveTEAM;

    private Team teamOne;
    private Team teamTwo;


    private Game mGame;

    private int maxScore = 10;

    public ScoreboardLogic(EggSplosion plugin, Game game) {
        mPlugin = plugin;
        mGame = game;
    }

    public void setMaxScore(int score) {
        maxScore = score;
    }

    public void addScore(int amount, Player player) {
        switch (mGame.getMatch().getMatchType()) {
            case LOBBY:
                break;
            case SOLO:
                joinHIDE();
                if (getScore(player) + amount >= maxScore) {
                    Score currentScore = scoreObjectiveSOLO.getScore(player.getDisplayName());
                    currentScore.setScore(currentScore.getScore() + amount);
                    winMatch(player);
                } else {
                    Score currentScore = scoreObjectiveSOLO.getScore(player.getDisplayName());
                    currentScore.setScore(currentScore.getScore() + amount);
                }
                break;
            case TEAM:
                if (getScore(player) + amount >= maxScore) {
                    Score currentScore = scoreObjectiveTEAM.getScore(getTeam(player).getName());
                    currentScore.setScore(currentScore.getScore() + amount);

                    winMatch(player);
                } else {
                    Score currentScore = scoreObjectiveTEAM.getScore(getTeam(player).getName());
                    currentScore.setScore(currentScore.getScore() + amount);
                }
                break;
        }
    }

    public final Team getTeamOne(){
        return teamOne;
    }

    public final Team getTeamTwo(){
        return teamTwo;
    }

    private void winMatch(Player player) {
        switch (mGame.getMatch().getMatchType()) {
            case SOLO:
                Collection<? extends Player> players = Bukkit.getOnlinePlayers();
                players.forEach(playerCurrent -> {
                    playerCurrent.sendTitle(ChatColor.GOLD + player.getDisplayName(), " has won the match!");
                });
                resetScoreboard();
                mGame.setLobby();
                break;
            case TEAM:
                Collection<? extends Player> playersTeam = Bukkit.getOnlinePlayers();
                playersTeam.forEach(currentPlayer -> {
                    currentPlayer.sendTitle(getTeam(player).getDisplayName(), " has won the match!");
                });
                resetScoreboard();
                mGame.removeFlags();
                mGame.setLobby();
        }
    }

    public void setScoreObjective() {
        switch (mGame.getMatch().getMatchType()){
            case SOLO:
                //Set SOLO ScoreObjective
                scoreObjectiveSOLO = gameBoardSOLO.registerNewObjective("Score", "dummy");
                scoreObjectiveSOLO.setDisplayName("Score");
                scoreObjectiveSOLO.setDisplaySlot(DisplaySlot.SIDEBAR);
                break;
            case TEAM:
                //Set TEAM ScoreObjective
                scoreObjectiveTEAM = gameBoardTEAM.registerNewObjective("Score", "dummy");
                scoreObjectiveTEAM.setDisplayName("Score");
                scoreObjectiveTEAM.setDisplaySlot(DisplaySlot.SIDEBAR);
        }
    }

    public int getScore(Player player) {
        switch (mGame.getMatch().getMatchType()) {
            case SOLO:
                return scoreObjectiveSOLO.getScore(player.getDisplayName()).getScore();
            case TEAM:
                if(teamOne.hasEntry(player.getDisplayName())){
                    return scoreObjectiveTEAM.getScore(teamOne.getName()).getScore();
                } else if (teamTwo.hasEntry(player.getDisplayName())){
                    return scoreObjectiveTEAM.getScore(teamTwo.getName()).getScore();
                } else {
                    return -1;
                }
            default:
                return -1;
        }
    }

    public void resetScoreboard() {
        switch (mGame.getMatch().getMatchType()) {
            case SOLO:
                //Reset SOLO ScoreBoard
                scoreObjectiveSOLO.unregister();
                for(Player player : Bukkit.getOnlinePlayers()) {
                    gameBoardSOLO.getTeam("hide").removeEntry(player.getDisplayName());
                }
                break;
            case TEAM:
                //Reset TEAM ScoreBoard
                scoreObjectiveTEAM.unregister();
                for(Player player : Bukkit.getOnlinePlayers()) {
                    gameBoardTEAM.getTeam(getTeam(player).getName()).removeEntry(player.getDisplayName());
                }

        }
    }

    public void assignTeams(){
        for (Player players : Bukkit.getOnlinePlayers()) {
            players.setScoreboard(gameBoardTEAM);
            if(gameBoardTEAM.getTeam(teamTwo.getName()).getSize() >= gameBoardTEAM.getTeam(teamOne.getName()).getSize()){
                //Add Player to Team ONE
                teamOne.addEntry(players.getDisplayName());
                System.out.println(teamOne);
                Bukkit.getLogger().info(players.getDisplayName() + "joined blue team!");
                colorArmor(players, Color.fromRGB(150, 52, 48));
            } else {
                //Add Player to Team TWO
                teamTwo.addEntry(players.getDisplayName());
                Bukkit.getLogger().info(players.getDisplayName() + "joined red team!");
                colorArmor(players, Color.fromRGB(46, 56, 141));
            }
        }
    }

    public void newScoreboard() {
        switch (mGame.getMatch().getMatchType()) {
            case SOLO:
                //Generate New SOLO ScoreBoard
                scoreboardManager = Bukkit.getScoreboardManager();
                gameBoardSOLO = scoreboardManager.getNewScoreboard();

                //Register HIDE
                gameBoardSOLO.registerNewTeam("hide");
                gameBoardSOLO.getTeam("hide").setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.NEVER);
                joinHIDE();

                break;
            case TEAM:
                //Generate New TEAM ScoreBoard
                scoreboardManager = Bukkit.getScoreboardManager();
                gameBoardTEAM = scoreboardManager.getNewScoreboard();

                registerTeams();
        }
    }

    private void joinHIDE(){
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(player.getGameMode() == GameMode.SURVIVAL) {
                player.setScoreboard(gameBoardSOLO);
                gameBoardSOLO.getTeam("hide").addEntry(player.getDisplayName());
            }
        }
    }

    public Team getTeam(Player player) {
        if(teamOne.hasEntry(player.getDisplayName())) {
            return teamOne;
        } else if (teamTwo.hasEntry(player.getDisplayName())){
            return  teamTwo;
        } else {
            return null;
        }
    }

    private void registerTeams(){
        //Register Teams
        teamOne = gameBoardTEAM.registerNewTeam(ChatColor.RED + "Red");
        teamTwo = gameBoardTEAM.registerNewTeam(ChatColor.BLUE + "Blue");

        //Set Team Names
        teamOne.setDisplayName(ChatColor. RED + "Red");
        teamTwo.setDisplayName(ChatColor.BLUE + "Blue");

        //Set Prefix Colors
        teamOne.setPrefix(ChatColor.RED + "");
        teamTwo.setPrefix(ChatColor.BLUE + "");
    }

    public void colorArmor(Player player, Color color){
        //Give Player Coloured Armor
        ItemStack chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE);
        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
        LeatherArmorMeta chestPlateMeta = (LeatherArmorMeta) chestPlate.getItemMeta();
        LeatherArmorMeta leggingMeta = (LeatherArmorMeta) leggings.getItemMeta();
        LeatherArmorMeta bootsMeta = (LeatherArmorMeta) boots.getItemMeta();

        EntityEquipment equipment = player.getEquipment();
        chestPlateMeta.setColor(color);
        leggingMeta.setColor(color);
        bootsMeta.setColor(color);

        chestPlate.setItemMeta(chestPlateMeta);
        leggings.setItemMeta(leggingMeta);
        boots.setItemMeta(bootsMeta);

        equipment.setChestplate(chestPlate);
        equipment.setLeggings(leggings);
        equipment.setBoots(boots);
    }
}
