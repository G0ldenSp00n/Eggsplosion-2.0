package me.g0ldensp00n.gamelogic;

import me.g0ldensp00n.gamelogic.gamemode.ModeType;
import me.g0ldensp00n.gamelogic.matchtype.MatchType;
import me.g0ldensp00n.main.EggSplosion;
import net.minecraft.server.v1_10_R1.Explosion;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.StringUtil;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class SpawnLogic implements Listener {

    private final Random randomSpawn = new Random();
    public List<Player> invulnerablePlayers = new ArrayList<>();
    private EggSplosion mPlugin;
    private Game mGame;
    private ScoreboardLogic mScoreboardLogic;

    //SOLO
    private Material toolSOLO = Material.GOLD_SPADE;
        // Spawn List
    private ArrayList<Location> spawnListSOLO = new ArrayList<>();
    private List<String> locationStringSOLO = new ArrayList<>();

    //TEAMS
    private Material toolOne = Material.IRON_SPADE;
    private Material toolTwo = Material.DIAMOND_SPADE;
        // Spawn Lists
            //Team One
    private ArrayList<Location> spawnListOne = new ArrayList<>();
    private List<String> locationStringOne = new ArrayList<>();
            //Team Two
    private ArrayList<Location> spawnListTwo = new ArrayList<>();
    private  List<String> locationStringTwo = new ArrayList<>();


    private String mArenaName = "NULL";
    private String mMatchType = "LOBBY";

    private long timeMilis;

    SpawnLogic(EggSplosion plugin, Game game, ScoreboardLogic scoreboardLogic) {
        mPlugin = plugin;
        mGame = game;
        mScoreboardLogic = scoreboardLogic;
        Bukkit.getPluginManager().registerEvents(this, mPlugin);
    }

    public void setArenaName(String arenaName, String matchType) {
        switch (matchType) {
            case "LOBBY":
                break;
            case "SOLO":
                if (spawnListSOLO.isEmpty()) {
                    mArenaName = arenaName;
                    mMatchType = matchType;
                } else {
                    spawnListSOLO = new ArrayList<>();
                    System.out.println(spawnListSOLO);
                    mArenaName = arenaName;
                    mMatchType = matchType;
                    mPlugin.getConfig().createSection("SOLO." + arenaName);
                    mPlugin.saveConfig();
                }
                break;
            case "TEAM":
                if(spawnListOne.isEmpty() && spawnListTwo.isEmpty()){
                    mArenaName = arenaName;
                    mMatchType = matchType;
                } else {
                    spawnListOne = new ArrayList<>();
                    spawnListTwo = new ArrayList<>();

                    mArenaName = arenaName;
                    mMatchType = matchType;
                    mPlugin.getConfig().createSection("TEAM." + arenaName + ".ONE");
                    mPlugin.getConfig().createSection("TEAM." + arenaName + ".TWO");
                    mPlugin.saveConfig();
                }

            default:
                break;
        }

    }

    public void loadArena(String arenaName, String matchType) {
        switch (matchType) {
            case "SOLO":
                mArenaName = arenaName;
                mMatchType = matchType;

                spawnListSOLO.clear();
                locationStringSOLO = mPlugin.getConfig().getStringList("SOLO." + arenaName);
                locationStringSOLO.forEach(locationString -> {
                    Location currentLoc = stringtoLocation(locationString);
                    spawnListSOLO.add(currentLoc);
                });
                break;
            case "TEAM":
                mArenaName = arenaName;
                mMatchType = matchType;

                spawnListOne.clear();
                spawnListTwo.clear();

                locationStringOne = mPlugin.getConfig().getStringList("TEAM." +arenaName + ".ONE");
                locationStringTwo = mPlugin.getConfig().getStringList("TEAM." +arenaName + ".TWO");
                locationStringOne.forEach(locationStringOneCurent -> {
                    Location currentLoc = stringtoLocation(locationStringOneCurent);
                    spawnListOne.add(currentLoc);
                });
                locationStringTwo.forEach(locationStringTwoCurrent -> {
                    Location currentLoc = stringtoLocation(locationStringTwoCurrent);
                    spawnListTwo.add(currentLoc);
                });
                break;
            default:
                break;
        }
    }

    public String getmArenaName(){
        return mArenaName;
    }

    public boolean checkArenaName(String arenaName, String matchType) {
        switch (matchType) {
            case "SOLO":
                return mPlugin.getConfig().contains("SOLO." + arenaName);
            case "TEAM":
                return mPlugin.getConfig().contains("TEAM." + arenaName + ".ONE");
            default:
                return false;
        }
    }

    public void spawnEvent(Player player) {
        switch (mGame.getMatch().getMatchType()) {
            case LOBBY:
                break;
            case SOLO:
                spawnHandling(spawnListSOLO, player, 10, false);
                break;
            case TEAM:
                if(mScoreboardLogic.getTeam(player).equals(mScoreboardLogic.getTeamOne())) {
                    spawnHandling(spawnListOne, player, 10, false);
                } else if (mScoreboardLogic.getTeam(player).equals(mScoreboardLogic.getTeamTwo())){
                    spawnHandling(spawnListTwo, player, 10, false);
                }
                break;
        }
    }

    public void spawnEvent(Player player, int respawnDelay, boolean start) {
        switch (mGame.getMatch().getMatchType()) {
            case LOBBY:
                break;
            case SOLO:
                spawnHandling(spawnListSOLO, player, respawnDelay, start);
                break;
            case TEAM:
                if(mScoreboardLogic.getTeam(player).equals(mScoreboardLogic.getTeamOne())) {
                    spawnHandling(spawnListOne, player, respawnDelay, start);
                } else if (mScoreboardLogic.getTeam(player).equals(mScoreboardLogic.getTeamTwo())){
                    spawnHandling(spawnListTwo, player, respawnDelay, start);
                }
                break;
        }
    }

    private void spawnHandling(List<Location> spawnList, Player player, int respawnDelay, boolean start){
        if (!spawnList.isEmpty()) {
            for (Player players : Bukkit.getOnlinePlayers()) {
                players.hidePlayer(player);
            }
            invulnerablePlayers.add(player);
            mPlugin.setInvulnerablePlayers(invulnerablePlayers);
            player.setHealth(20);
            player.setFoodLevel(20);
            player.setVelocity(new Vector(0, 0, 0));
            player.setFireTicks(0);
            player.getEquipment().setHelmet(null);
            int spawnNum = randomSpawn.nextInt(spawnList.size());
            player.teleport((spawnList.get(spawnNum)));
            new BukkitRunnable() {
                float maxXP = 1.0F;
                float divideXP = maxXP / respawnDelay;

                @Override
                public void run() {
                    if (maxXP > 0) {
                        player.setExp(maxXP);
                        maxXP = maxXP - divideXP;
                    } else {
                        player.setExp(0);
                        cancel();
                    }
                }
            }.runTaskTimer(mPlugin, 0, 1);
            new BukkitRunnable() {
                @Override
                public void run() {
                    for (Player players : Bukkit.getOnlinePlayers()) {
                        players.showPlayer(player);
                    }
                    invulnerablePlayers.remove(player);
                    mPlugin.setInvulnerablePlayers(invulnerablePlayers);
                }
            }.runTaskLater(mPlugin, respawnDelay);
            if (start) {
                new BukkitRunnable() {
                    int secCounter = 5;

                    public void run() {
                        if (secCounter >= 1) {
                            player.sendTitle(ChatColor.GOLD + "Match starts in", ChatColor.GOLD + " " + secCounter + " ");
                            secCounter--;
                        } else {
                            player.sendTitle(ChatColor.GOLD + "GO!" , "");
                            cancel();
                        }
                    }
                }.runTaskTimer(mPlugin, 0, 20);
            }
        } else {
            noSpawnSet(player);
        }
    }

    private void noSpawnSet(Player player) {
        player.teleport(player.getWorld().getSpawnLocation());
        Bukkit.getLogger().warning("No Spawns are Set, Please set some!!");
    }

    @EventHandler
    private void regenFood(FoodLevelChangeEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void setSpawn(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.getEquipment().getItemInMainHand().getType().equals(toolSOLO) && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (timeMilis + 50 < System.currentTimeMillis()) {
                if (mArenaName.equalsIgnoreCase("NULL")) {
                    player.sendMessage("No arena is set, set one with the /arena command.");
                } else {
                    if (mMatchType.equals("SOLO")) {
                        Location spawnLoc = event.getClickedBlock().getLocation();
                        spawnLoc.add(0, 1, 0);
                        player.sendMessage("Spawn " + (spawnListSOLO.size() + 1) + " added at - X: " + spawnLoc.getX() + " Y:" + spawnLoc.getY() + " Z:" + spawnLoc.getZ());
                        spawnListSOLO.add(spawnLoc);
                        locationStringSOLO.add(locationToString(spawnLoc));
                        mPlugin.getConfig().set("SOLO." + mArenaName, locationStringSOLO);
                        mPlugin.saveConfig();
                    }

                }
                timeMilis = System.currentTimeMillis();
            }
        }
        if (player.getEquipment().getItemInMainHand().getType().equals(toolOne) && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (timeMilis + 50 < System.currentTimeMillis()) {
                if (mArenaName.equalsIgnoreCase("NULL")) {
                    player.sendMessage("No arena is set, set one with the /arena command.");
                } else {
                    if (mMatchType.equals("TEAM")) {
                        Location spawnLoc = event.getClickedBlock().getLocation();
                        spawnLoc.add(0, 1, 0);
                        player.sendMessage("Spawn " + (spawnListOne.size() + 1) + " for TEAM:ONE" + " added at - X: " + spawnLoc.getX() + " Y:" + spawnLoc.getY() + " Z:" + spawnLoc.getZ());
                        spawnListOne.add(spawnLoc);
                        locationStringOne.add(locationToString(spawnLoc));
                        mPlugin.getConfig().set("TEAM." + mArenaName + ".ONE", locationStringOne);
                        mPlugin.saveConfig();
                    }
                }
                timeMilis = System.currentTimeMillis();
            }
        }
        if (player.getEquipment().getItemInMainHand().getType().equals(toolTwo) && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (timeMilis + 50 < System.currentTimeMillis()) {
                if (mArenaName.equalsIgnoreCase("NULL")) {
                    player.sendMessage("No arena is set, set one with the /arena command.");
                } else {
                    if (mMatchType.equals("TEAM")) {
                        Location spawnLoc = event.getClickedBlock().getLocation();
                        spawnLoc.add(0, 1, 0);
                        player.sendMessage("Spawn " + (spawnListTwo.size() + 1) + " for TEAM:TWO" + " added at - X: " + spawnLoc.getX() + " Y:" + spawnLoc.getY() + " Z:" + spawnLoc.getZ());
                        spawnListTwo.add(spawnLoc);
                        locationStringTwo.add(locationToString(spawnLoc));
                        mPlugin.getConfig().set("TEAM." + mArenaName + ".TWO", locationStringTwo);
                        mPlugin.saveConfig();
                    }
                }
                timeMilis = System.currentTimeMillis();
            }
        }
    }

    @EventHandler
    public void playerFreeze(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (invulnerablePlayers.contains(player)) {
            if (event.getFrom().getX() != event.getTo().getX() || event.getFrom().getY() != event.getTo().getY() || event.getFrom().getZ() != event.getTo().getZ()) {
                Location from = new Location(player.getWorld(), event.getFrom().getX(), event.getFrom().getY(),
                        event.getFrom().getZ(), event.getTo().getYaw(), event.getTo().getPitch());
                player.teleport(from);
            }
        }
    }

    @EventHandler
    public void playerDing(EntityDamageByBlockEvent event){
        if(event.getEntity() instanceof Player){
            Player player = (Player) event.getEntity();
            if(event.getDamager() instanceof Explosion){
                TNTPrimed damager = (TNTPrimed) event.getDamager();
                if(damager.getPassenger() instanceof Snowball){
                    Collection<? extends org.bukkit.entity.Player> players = Bukkit.getOnlinePlayers();
                    players.forEach(damgerPlayer -> {
                        if(damgerPlayer.getUniqueId().toString() == damager.getName()){
                            damgerPlayer.playSound(damgerPlayer.getLocation(), Sound.ENTITY_BLAZE_HURT, 1, 1);
                        }
                    });
                }
            }
        }
    }

    public void printSpawns(CommandSender cmdSender) {
        switch (mGame.getMatch().getMatchType()) {
            case LOBBY:
                cmdSender.sendMessage("Lobby - ");
            case SOLO:
                for (int i = 0; i < spawnListSOLO.size(); i++) {
                    Location spawnLoc = spawnListSOLO.get(i);
                    cmdSender.sendMessage("Spawn " + i + " - X: " + spawnLoc.getX() + " Y:" + spawnLoc.getY() + " Z:" + spawnLoc.getZ());
                }
                break;
            case TEAM:
                for (int i = 0; i < spawnListOne.size(); i++) {
                    Location spawnLoc = spawnListOne.get(i);
                    cmdSender.sendMessage("Spawn (Team 1)" + (i+1) + " - X: " + spawnLoc.getX() + " Y:" + spawnLoc.getY() + " Z:" + spawnLoc.getZ());
                }
                for (int i = 0; i < spawnListTwo.size(); i++) {
                    Location spawnLoc = spawnListTwo.get(i);
                    cmdSender.sendMessage("Spawn (Team 2)" + (i+1) + " - X: " + spawnLoc.getX() + " Y:" + spawnLoc.getY() + " Z:" + spawnLoc.getZ());
                }
                break;
            default:
                cmdSender.sendMessage("It appear that no Match Type has been set, please file a bug report!");
        }
    }

    private String locationToString(Location location) {
        return location.getWorld().getName() + " " + location.getX() + " " + location.getY() + " " + location.getZ();
    }

    private Location stringtoLocation(String location) {
        String[] locationString = location.split(" ");
        if (locationString.length >= 4) {
            World world = Bukkit.getWorld(locationString[0]);
            try {
                double x = Double.parseDouble(locationString[1]);
                double y = Double.parseDouble(locationString[2]);
                double z = Double.parseDouble(locationString[3]);

                return new Location(world, x, y, z);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    @EventHandler
    public void joinGame(PlayerJoinEvent event){
        if(mGame.getMode().getModeType() == ModeType.LOBBY) {
            mGame.restoreInventory(event.getPlayer());
        }
    }

    @EventHandler
    public void playerPickup(PlayerPickupItemEvent event){
            if(event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
    }

    @EventHandler
    public void playerDrop(PlayerDropItemEvent event){
            if(event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
    }

    @EventHandler
    public void playerPlace(BlockPlaceEvent event) {
            if (event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
    }

    @EventHandler
    public void playerBreak(BlockBreakEvent event){
            if(event.getPlayer().getGameMode() != GameMode.CREATIVE){
                event.setCancelled(true);
            }
    }
}
