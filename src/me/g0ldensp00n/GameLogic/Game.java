package me.g0ldensp00n.gamelogic;

import me.g0ldensp00n.gamelogic.commandhandler.GameCommandHandler;
import me.g0ldensp00n.gamelogic.commandhandler.SpawnCommandHandler;
import me.g0ldensp00n.gamelogic.gamemode.Mode;
import me.g0ldensp00n.gamelogic.gamemode.ModeType;
import me.g0ldensp00n.gamelogic.matchtype.Match;
import me.g0ldensp00n.gamelogic.modelogic.CTFLogic;
import me.g0ldensp00n.gamelogic.modelogic.FFALogic;
import me.g0ldensp00n.gamelogic.modelogic.LobbyLogic;
import me.g0ldensp00n.gamelogic.modelogic.TDMLogic;
import me.g0ldensp00n.handlers.KillMessageHandler;
import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Game {

    private EggSplosion mPlugin;
    private Mode mMode;
    private Match mMatch;


    private SpawnLogic spawnLogic;
    private ScoreboardLogic scoreboardLogic;

    private SpawnCommandHandler spawnCommandHandler;
    private GameCommandHandler gameCommandHandler;

    private FFALogic mFFALogic;
    private TDMLogic mTDMLogic;
    private CTFLogic mCTFLogic;
    private LobbyLogic mLobbyLogic;

    private KillMessageHandler mKillMessageHandler;

    private Map<UUID, ItemStack[]> playerInventoryStorage = new HashMap<>();
    private Map<UUID, ItemStack[]> playerArmorStorage = new HashMap<>();

    private Material hoeType;

    public Game(EggSplosion plugin) {
        mPlugin = plugin;
        mMode = new Mode(ModeType.LOBBY);
        mMatch = new Match(mMode.getMatchType());

        registerLogic();
        registerModeLogic();

        mKillMessageHandler = new KillMessageHandler();
        spawnCommandHandler = new SpawnCommandHandler(plugin, spawnLogic);
        gameCommandHandler = new GameCommandHandler(plugin, this, spawnLogic);
        plugin.getCommand("arena").setExecutor(spawnCommandHandler);
        plugin.getCommand("spawn").setExecutor(spawnCommandHandler);
        plugin.getCommand("game").setExecutor(gameCommandHandler);
    }

    public void setLobby() {
        restoreInventories();
        mMode = new Mode(ModeType.LOBBY);
        mMatch = new Match(mMode.getMatchType());
    }

    public void setMode(Mode mode, int score, Material hoeType) {
        mMode = mode;
        mMatch = new Match(mode.getMatchType());
        scoreboardLogic.setMaxScore(score);
        switch (mode.getModeType()) {
            case FFA:
                mFFALogic.start();
                break;
            case TDM:
                mTDMLogic.start();
                break;
            case CTF:
                this.hoeType = hoeType;
                mCTFLogic.start();
                break;
        }
    }

    private void registerLogic() {
        scoreboardLogic = new ScoreboardLogic(mPlugin, this);
        spawnLogic = new SpawnLogic(mPlugin, this, scoreboardLogic);
    }

    private void registerModeLogic() {
        mLobbyLogic = new LobbyLogic(mPlugin, this, spawnLogic);
        mFFALogic = new FFALogic(mPlugin, this, spawnLogic, scoreboardLogic);
        mTDMLogic = new TDMLogic(mPlugin, this, spawnLogic, scoreboardLogic);
        mCTFLogic = new CTFLogic(mPlugin, scoreboardLogic, spawnLogic, mKillMessageHandler);
    }

    public void setupInventories(){
            Collection<? extends Player> players = Bukkit.getOnlinePlayers();
            players.forEach(player -> {
                if(player.getGameMode() != GameMode.CREATIVE){
                    playerInventoryStorage.put(player.getUniqueId(), player.getInventory().getContents());
                    playerArmorStorage.put(player.getUniqueId(), player.getInventory().getArmorContents());
                    player.getInventory().clear();
                    player.getInventory().addItem(new ItemStack(hoeType, 1));
                }
            });
    }

    public void restoreInventories(){
        Collection<? extends Player> players = Bukkit.getOnlinePlayers();
        players.forEach(player -> {
           if(playerInventoryStorage.containsKey(player.getUniqueId())){
               player.getInventory().clear();
               player.getInventory().setContents(playerInventoryStorage.get(player.getUniqueId()));
               player.getInventory().setArmorContents(playerArmorStorage.get(player.getUniqueId()));
           }
        });
    }

    public void restoreInventory(Player player){
        if(playerInventoryStorage.containsKey(player.getUniqueId())){
            player.getInventory().clear();
            player.getInventory().setContents(playerInventoryStorage.get(player.getUniqueId()));
            player.getInventory().setArmorContents(playerArmorStorage.get(player.getUniqueId()));
        }
    }

    public void removeFlags(){
        if(mMode.getModeType() == ModeType.CTF){
            mCTFLogic.removeFlags();
        }
    }

    public Match getMatch() {
        return mMatch;
    }

    public Mode getMode() {
        return mMode;
    }

    public Material getHoeType(){
        return hoeType;
    }

}
