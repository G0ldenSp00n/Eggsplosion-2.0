package me.g0ldensp00n.gamelogic.commandhandler;

import me.g0ldensp00n.gamelogic.SpawnLogic;
import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SpawnCommandHandler implements CommandExecutor {

    private EggSplosion mPlugin;
    private SpawnLogic mSpawnLogic;

    public SpawnCommandHandler(EggSplosion plugin, SpawnLogic spawnLogic) {
        mPlugin = plugin;
        mSpawnLogic = spawnLogic;
    }


    @Override
    public boolean onCommand(CommandSender cmdSender, Command command, String commandLabel, String[] args) {
        if (commandLabel.equalsIgnoreCase("arena")) {
            if (args.length > 2) {
                if (args[0].equalsIgnoreCase("set")) {
                    if (!mSpawnLogic.checkArenaName(args[1], args[2])) {
                        mSpawnLogic.setArenaName(args[1], args[2]);
                        cmdSender.sendMessage("Arena set to " + args[1] + " for GameType: " + args[2]);
                    } else {
                        cmdSender.sendMessage("Arena " + args[1] + " already exists!  Load it with /arena load " + args[1]);
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("load")) {
                    if (mSpawnLogic.checkArenaName(args[1], args[2])) {
                        mSpawnLogic.loadArena(args[1], args[2]);
                        cmdSender.sendMessage("Arena " + args[1] + " loaded!");
                        return true;
                    } else {
                        cmdSender.sendMessage("The selected arena " + args[1] + " does not exist!");
                        return true;
                    }
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } else if (commandLabel.equalsIgnoreCase("spawn")) {
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("list")) {
                    mSpawnLogic.printSpawns(cmdSender);
                    return true;
                } else if(args[0].equalsIgnoreCase("tools")){
                    if(cmdSender instanceof Player){
                        Player player = (Player) cmdSender;
                        player.getInventory().addItem(new ItemStack(Material.IRON_AXE, 1), new ItemStack(Material.IRON_SPADE, 1), new ItemStack(Material.DIAMOND_SPADE, 1), new ItemStack(Material.DIAMOND_AXE, 1));
                        player.sendMessage("Use the Spades to set both team spawns, and the axes to set flag spawns!");
                        return true;
                    } else {
                        cmdSender.sendMessage("This command must be preformed by a player!");
                    }
                }
            }
        }
        return false;
    }
}
