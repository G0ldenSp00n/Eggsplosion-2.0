package me.g0ldensp00n.gamelogic.commandhandler;

import me.g0ldensp00n.gamelogic.Game;
import me.g0ldensp00n.gamelogic.SpawnLogic;
import me.g0ldensp00n.gamelogic.gamemode.Mode;
import me.g0ldensp00n.gamelogic.gamemode.ModeType;
import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class GameCommandHandler implements CommandExecutor {

    private EggSplosion mPlugin;
    private Game mGame;
    private SpawnLogic mSpawnLogic;

    private Material hoeType;

    public GameCommandHandler(EggSplosion plugin, Game game, SpawnLogic spawnLogic) {
        mPlugin = plugin;
        mGame = game;
        mSpawnLogic = spawnLogic;
    }

    @Override
    public boolean onCommand(CommandSender cmdSender, Command command, String commandLabel, String[] args) {
        if (commandLabel.equalsIgnoreCase("game")) {
            if (args.length > 3) {
                if (args[0].equalsIgnoreCase("start")) {
                    try {
                        if (args[3].equalsIgnoreCase("stone")){
                            hoeType = Material.STONE_HOE;
                        } else if (args[3].equalsIgnoreCase("iron")){
                            hoeType = Material.IRON_HOE;
                        } else if (args[3].equalsIgnoreCase("gold")){
                            hoeType = Material.GOLD_HOE;
                        } else if (args[3].equalsIgnoreCase("diamond")){
                            hoeType = Material.DIAMOND_HOE;
                        } else {
                            hoeType = Material.WOOD_HOE;
                        }

                        return startMatch(args[1], cmdSender, Integer.parseInt(args[2]), hoeType);

                    } catch (NumberFormatException e) {
                        return false;
                    }
                }
            } else if (args.length > 2){
                return startMatch(args[1], cmdSender, Integer.parseInt(args[2]), Material.WOOD_HOE);
            }
        }
        return false;
    }

    public boolean startMatch(String matchType, CommandSender cmdSender, int scoreLimit, Material type){
        switch (matchType) {
            case "FFA":
                if(mSpawnLogic.getmArenaName() != null) {
                    cmdSender.sendMessage("Game " + ModeType.FFA.getDisplayName() + " has been started, with a score limit of " + scoreLimit + " and hoe type " + type.name());
                    mGame.setMode(new Mode(ModeType.FFA), scoreLimit, type);
                    return true;
                } else {
                    cmdSender.sendMessage("There is no current arena, set one with the /arena command!");
                    return true;
                }
            case "TDM":
                if(mSpawnLogic.getmArenaName() != null) {
                    cmdSender.sendMessage("Game " + ModeType.TDM.getDisplayName() + " has been started, with a score limit of " + scoreLimit + " and hoe type " + type.name());
                    mGame.setMode(new Mode(ModeType.TDM), scoreLimit, type);
                    return true;
                } else {
                    cmdSender.sendMessage("There is no current arena, set one with the /arena command!");
                    return true;
                }
            case "CTF":
                if(mSpawnLogic.getmArenaName() != null) {
                    cmdSender.sendMessage("Game " + ModeType.CTF.getDisplayName() + " has been started, with a score limit of " + scoreLimit + " and hoe type " + type.name());
                    mGame.setMode(new Mode(ModeType.CTF), scoreLimit, type);
                    return true;
                } else {
                    cmdSender.sendMessage("There is no current arena, set one with the /arena command!");
                    return true;
                }
            default:
                return false;
        }
    }
}
