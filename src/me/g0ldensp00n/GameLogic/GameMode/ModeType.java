package me.g0ldensp00n.gamelogic.gamemode;

import me.g0ldensp00n.gamelogic.matchtype.MatchType;

public enum ModeType {
    LOBBY("Lobby", MatchType.LOBBY),
    FFA("Free for All", MatchType.SOLO),
    TDM("Team Death Match", MatchType.TEAM),
    CTF("Capture the Flag", MatchType.TEAM);

    private String mDisplayName;
    private MatchType mMatchType;

    ModeType(String displayName, MatchType matchType) {
        mDisplayName = displayName;
        mMatchType = matchType;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public MatchType getMatchType() {
        return mMatchType;
    }
}