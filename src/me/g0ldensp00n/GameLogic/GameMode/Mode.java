package me.g0ldensp00n.gamelogic.gamemode;


import me.g0ldensp00n.gamelogic.matchtype.MatchType;

public class Mode {

    private me.g0ldensp00n.gamelogic.gamemode.ModeType mModeType;
    private String mDisplayName;
    private MatchType mMatchType;

    public Mode(me.g0ldensp00n.gamelogic.gamemode.ModeType modeType) {
        mModeType = modeType;
        mDisplayName = mModeType.getDisplayName();
        mMatchType = mModeType.getMatchType();
    }

    public ModeType getModeType() {
        return mModeType;
    }

    public MatchType getMatchType() {
        return mMatchType;
    }

    public String getDisplayName() {
        return mDisplayName;
    }
}