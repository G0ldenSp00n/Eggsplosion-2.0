package me.g0ldensp00n.gamelogic.modelogic;

import me.g0ldensp00n.gamelogic.Game;
import me.g0ldensp00n.gamelogic.SpawnLogic;
import me.g0ldensp00n.gamelogic.gamemode.ModeType;
import me.g0ldensp00n.gamelogic.matchtype.MatchType;
import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class LobbyLogic implements Listener {

    EggSplosion mPlugin;
    Game mGame;
    SpawnLogic mSpawnLogic;

    public LobbyLogic(EggSplosion plugin, Game game, SpawnLogic spawnLogic) {
        mPlugin = plugin;
        mGame = game;
        mSpawnLogic = spawnLogic;

        Bukkit.getPluginManager().registerEvents(this, mPlugin);
    }

    @EventHandler
    public void playerMove(PlayerMoveEvent event) {
        if (mGame.getMatch().getMatchType() == MatchType.LOBBY) {
            Player player = event.getPlayer();
            if (player.getLocation().getY() < 1) {
                mSpawnLogic.spawnEvent(player);
            }
        }
    }

    @EventHandler
    public void playerDamage(EntityDamageEvent event){
        if (mGame.getMatch().getMatchType() == MatchType.LOBBY) {
            if (event.getEntity() instanceof Player) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void playerPickup(PlayerPickupItemEvent event){
        if(mGame.getMode().getModeType() == ModeType.LOBBY){
            if(event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void playerDrop(PlayerDropItemEvent event){
        if(mGame.getMode().getModeType() == ModeType.LOBBY){
            if(event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
        }
    }
}
