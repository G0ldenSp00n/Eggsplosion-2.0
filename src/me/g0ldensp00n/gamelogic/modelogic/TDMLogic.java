package me.g0ldensp00n.gamelogic.modelogic;

import me.g0ldensp00n.gamelogic.Game;
import me.g0ldensp00n.gamelogic.ScoreboardLogic;
import me.g0ldensp00n.gamelogic.SpawnLogic;
import me.g0ldensp00n.gamelogic.gamemode.ModeType;
import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TDMLogic implements Listener{

    private EggSplosion mPlugin;
    private Game mGame;
    private SpawnLogic mSpawnLogic;
    private ScoreboardLogic mScoreboardLogic;

    private Map<UUID, UUID> killHandlingMap = new HashMap<>();


    public TDMLogic(EggSplosion plugin, Game game, SpawnLogic spawnLogic, ScoreboardLogic scoreboardLogic) {
        mPlugin = plugin;
        mGame = game;
        mSpawnLogic = spawnLogic;
        mScoreboardLogic = scoreboardLogic;

        Bukkit.getPluginManager().registerEvents(this, mPlugin);
    }

    public void start() {
        mScoreboardLogic.newScoreboard();
        mScoreboardLogic.setScoreObjective();
        mScoreboardLogic.assignTeams();
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getGameMode() == GameMode.SURVIVAL) {
                mSpawnLogic.spawnEvent(player, 5 * 20, true);
            }
        }
    }

    private void scoreHandling(Player player) {
        mScoreboardLogic.addScore(1, player);
    }

    @EventHandler
    public void playerDeath(EntityDamageEvent event) {
        if (mGame.getMode().getModeType().equals(ModeType.TDM)) {
            if (event.getEntity() instanceof Player) {
                Player player = (Player) event.getEntity();
                if (player.getHealth() - event.getDamage() <= 0) {
                    event.setCancelled(true);
                    Collection<? extends Player> players = Bukkit.getOnlinePlayers();
                    players.forEach(playerDamager -> {
                        if (killHandlingMap.containsKey(player.getUniqueId())) {
                            if (killHandlingMap.containsValue(playerDamager.getUniqueId())) {
                                if (player != playerDamager) {
                                    scoreHandling(playerDamager);
                                }
                            }
                        }
                        killHandlingMap.remove(player.getUniqueId());
                    });
                    mSpawnLogic.spawnEvent(player);
                }
            }
        }
    }

    @EventHandler
    public void playerScore(EntityDamageByEntityEvent event) {
        if (mGame.getMode().getModeType().equals(ModeType.TDM)) {
            if (event.getEntity() instanceof Player) {
                Player player = (Player) event.getEntity();
                if (event.getDamager() instanceof TNTPrimed) {
                    if (event.getDamager().getPassenger() instanceof Snowball) {
                        UUID pID = UUID.fromString(event.getDamager().getCustomName());
                        Collection<? extends org.bukkit.entity.Player> players = Bukkit.getOnlinePlayers();
                        players.forEach(playerDamager -> {
                            if (playerDamager.getUniqueId().equals(pID)) {
                                    if (killHandlingMap.containsKey(player.getUniqueId())) {
                                        killHandlingMap.replace(player.getUniqueId(), killHandlingMap.get(player.getUniqueId()), playerDamager.getUniqueId());
                                        System.out.println("Player " +player.getDisplayName() + " (" +player.getUniqueId() + ")" + " -> Damger " + player.getDisplayName() + " (" + killHandlingMap.get(player.getUniqueId()) + ")");
                                        System.out.println("Replace: " + playerDamager.getDisplayName() + " Killed " + player.getDisplayName());
                                    } else {
                                        killHandlingMap.put(player.getUniqueId(), playerDamager.getUniqueId());
                                        System.out.println(playerDamager.getDisplayName() + " Killed " + player.getDisplayName());
                                    }
                                }
                        });
                    }
                }
            }
        }
    }
}
