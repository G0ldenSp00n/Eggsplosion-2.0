package me.g0ldensp00n.gamelogic.modelogic;

import me.g0ldensp00n.gamelogic.ScoreboardLogic;
import me.g0ldensp00n.gamelogic.SpawnLogic;
import me.g0ldensp00n.gamelogic.gamemode.ModeType;
import me.g0ldensp00n.handlers.KillMessageHandler;
import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_10_R1.block.CraftBanner;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import java.util.*;

public class CTFLogic implements Listener {

    private EggSplosion plugin;
    private ScoreboardLogic mScoreboardLogic;
    private final SpawnLogic mSpawnLogic;
    private final KillMessageHandler mKillMessageHandler;

    private Map<Team, Location> flagLocation = new HashMap<>();
    private Map<Integer, Location> tempFlagLocation = new HashMap<>();
    private List<UUID> flagHolder = new ArrayList<>();

    private Material teamOne = Material.IRON_AXE;
    private Material teamTwo = Material.DIAMOND_AXE;

    private long timeMilis;

    public CTFLogic(EggSplosion plugin, ScoreboardLogic scoreboardLogic, SpawnLogic spawnLogic, KillMessageHandler killMessageHandler) {
        this.plugin = plugin;
        this.mScoreboardLogic = scoreboardLogic;
        this.mSpawnLogic = spawnLogic;
        this.mKillMessageHandler = killMessageHandler;
        Bukkit.getPluginManager().registerEvents(this, this.plugin);
    }

    public void start() {
        plugin.getGame().setupInventories();

        mScoreboardLogic.newScoreboard();
        mScoreboardLogic.setScoreObjective();
        mScoreboardLogic.assignTeams();

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getGameMode() == GameMode.SURVIVAL) {
                mSpawnLogic.spawnEvent(player, 5 * 20, true);
            }
        }
        if (tempFlagLocation.containsKey(1)) {
            flagLocation.put(mScoreboardLogic.getTeamOne(), tempFlagLocation.get(1));
            if (flagLocation.get(mScoreboardLogic.getTeamOne()) != null) {
                spawnFlag(flagLocation.get(mScoreboardLogic.getTeamOne()), mScoreboardLogic.getTeamOne());
            }
        } else {
            System.out.println("No flags are set, set some with a " + teamTwo.name());
        }
        if (tempFlagLocation.containsKey(2)) {
            flagLocation.put(mScoreboardLogic.getTeamTwo(), tempFlagLocation.get(2));
            if (flagLocation.get(mScoreboardLogic.getTeamTwo()) != null) {
                spawnFlag(flagLocation.get(mScoreboardLogic.getTeamTwo()), mScoreboardLogic.getTeamTwo());
            }
        } else {
            System.out.println("No flags are set, set some with a " + teamOne.name());
        }
    }



    @EventHandler
    public void locationFlag(PlayerInteractEvent event) {
        Player player = event.getPlayer();
            if (player.getEquipment().getItemInMainHand().getType().equals(teamTwo) && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                if (timeMilis + 50 < System.currentTimeMillis()) {
                Location location = event.getClickedBlock().getLocation();
                location.add(0, 1, 0);
                if (tempFlagLocation.containsKey(2)) {
                    tempFlagLocation.replace(2, location);
                    System.out.println("Replace Flag: " + location);
                    System.out.println(flagLocation);
                } else {
                    tempFlagLocation.put(2, location);
                    System.out.println("Put Flag: " + location);
                    System.out.println(flagLocation);
                }
                player.sendMessage("Flag Location Saved!");
                    timeMilis = System.currentTimeMillis();
            }
        } else if (player.getEquipment().getItemInMainHand().getType().equals(teamOne) && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                if (timeMilis + 50 < System.currentTimeMillis()) {
                    Location location = event.getClickedBlock().getLocation();
                    location.add(0, 1, 0);
                    if (tempFlagLocation.containsKey(1)) {
                        tempFlagLocation.replace(1, location);
                    } else {
                        tempFlagLocation.put(1, location);
                    }
                    player.sendMessage("Flag Location Saved!");
                    timeMilis = System.currentTimeMillis();
                }
        }
    }

    @EventHandler
    public void captureFlag(PlayerInteractEvent event) {
        if (plugin.getGame().getMode().getModeType() == ModeType.CTF) {
            if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                if (event.getClickedBlock().getType().equals(Material.STANDING_BANNER)) {
                    CraftBanner craftBanner = new CraftBanner(event.getClickedBlock().getLocation().getBlock());
                    org.bukkit.material.Banner banner = new org.bukkit.material.Banner();
                    ItemStack bannerItem = banner.toItemStack();
                    bannerItem.setAmount(1);
                    if (craftBanner.getBaseColor().equals(DyeColor.BLUE)) {
                        Player player = event.getPlayer();
                        if (mScoreboardLogic.getTeam(player) == mScoreboardLogic.getTeamOne()) {
                            BannerMeta bannerMeta = (BannerMeta) bannerItem.getItemMeta();
                            bannerMeta.setBaseColor(DyeColor.BLUE);
                            bannerItem.setItemMeta(bannerMeta);
                            player.getEquipment().setHelmet(bannerItem);
                            flagHolder.add(player.getUniqueId());
                            flagPoint(player);
                            Collection<? extends Player> players = Bukkit.getOnlinePlayers();
                            players.forEach(playerCurrent -> {
                                playerCurrent.sendMessage(ChatColor.RED + player.getDisplayName() + ChatColor.RESET + " has stolen the" + ChatColor.BLUE + " Blue Flag!");
                            });
                            event.getClickedBlock().getLocation().getBlock().setType(Material.AIR);
                        } else {
                            event.setCancelled(true);
                        }
                    } else if (craftBanner.getBaseColor().equals(DyeColor.RED)) {
                        Player player = event.getPlayer();
                        if (mScoreboardLogic.getTeam(player) == mScoreboardLogic.getTeamTwo()) {
                            BannerMeta bannerMeta = (BannerMeta) bannerItem.getItemMeta();
                            bannerMeta.setBaseColor(DyeColor.RED);
                            bannerItem.setItemMeta(bannerMeta);
                            player.getEquipment().setHelmet(bannerItem);
                            flagHolder.add(player.getUniqueId());
                            flagPoint(player);
                            Collection<? extends Player> players = Bukkit.getOnlinePlayers();
                            players.forEach(playerCurrent -> {
                                playerCurrent.sendMessage(ChatColor.BLUE + player.getDisplayName() + ChatColor.RESET + " has stolen the" + ChatColor.RED + " Red Flag!");
                            });
                            event.getClickedBlock().getLocation().getBlock().setType(Material.AIR);
                        } else {
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    private boolean hasFlag(Player player) {
        if (plugin.getGame().getMode().getModeType() == ModeType.CTF) {
            if (flagHolder.contains(player.getUniqueId())) {
                flagHolder.remove(player.getUniqueId());
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private void flagPoint(Player player){
        if(plugin.getGame().getMode().getModeType() == ModeType.CTF) {
            if (flagHolder.contains(player.getUniqueId())) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (flagHolder.contains(player.getUniqueId())) {
                            if (player.getLocation().distance(flagLocation.get(mScoreboardLogic.getTeam(player))) < 2) {
                                mScoreboardLogic.addScore(1, player);
                                Collection<? extends Player> players = Bukkit.getOnlinePlayers();
                                players.forEach(playerCurrent -> {
                                    if (mScoreboardLogic.getTeam(player) == mScoreboardLogic.getTeamOne()) {
                                        playerCurrent.sendMessage(ChatColor.RED + player.getDisplayName() + ChatColor.RESET + " has captured the" + ChatColor.BLUE + " Blue Flag!");
                                    }
                                    if (mScoreboardLogic.getTeam(player) == mScoreboardLogic.getTeamTwo()) {
                                        playerCurrent.sendMessage(ChatColor.BLUE + player.getDisplayName() + ChatColor.RESET + " has captured the" + ChatColor.RED + " Red Flag!");
                                    }
                                    if (flagHolder.contains(playerCurrent.getUniqueId())) {
                                        flagHolder.remove(playerCurrent.getUniqueId());
                                        ItemStack air = playerCurrent.getEquipment().getItemInMainHand();
                                        air.setType(Material.AIR);
                                        air.setAmount(1);
                                        playerCurrent.getEquipment().setHelmet(air);
                                        respawnFlag(mScoreboardLogic.getTeam(playerCurrent));
                                    }
                                });
                            }
                        } else {
                            cancel();
                        }
                    }
                }.runTaskTimer(this.plugin, 10, 10);
            }
        }
    }

    private void spawnFlag(Location location, Team team) {
        System.out.println(location);
        if(plugin.getGame().getMode().getModeType() == ModeType.CTF) {
            if (team == mScoreboardLogic.getTeamOne()) {
                location.getBlock().setType(Material.STANDING_BANNER);
                CraftBanner banner = new CraftBanner(location.getBlock());
                banner.setBaseColor(DyeColor.RED);
                banner.update(true);
            } else if (team == mScoreboardLogic.getTeamTwo()) {
                location.getBlock().setType(Material.STANDING_BANNER);
                CraftBanner banner = new CraftBanner(location.getBlock());
                banner.setBaseColor(DyeColor.BLUE);
                banner.update(true);
            }
        }
    }

    private void respawnFlag(Team team) {
        if(plugin.getGame().getMode().getModeType() == ModeType.CTF) {
            if (team == mScoreboardLogic.getTeamTwo()) {
                Location location = flagLocation.get(mScoreboardLogic.getTeamOne());
                location.getBlock().setType(Material.STANDING_BANNER);
                CraftBanner banner = new CraftBanner(location.getBlock());
                banner.setBaseColor(DyeColor.RED);
                banner.update(true);
            }
            if (team == mScoreboardLogic.getTeamOne()) {
                Location location = flagLocation.get(mScoreboardLogic.getTeamTwo());
                location.getBlock().setType(Material.STANDING_BANNER);
                CraftBanner banner = new CraftBanner(location.getBlock());
                banner.setBaseColor(DyeColor.BLUE);
                banner.update(true);
            }
        }
    }

    public void removeFlags(){
        if(tempFlagLocation.get(1) != null) {
            Location flagRed = tempFlagLocation.get(1);
            flagRed.getBlock().setType(Material.AIR);
        }
        if(tempFlagLocation.get(2) != null) {
            Location flagBlue = tempFlagLocation.get(2);
            flagBlue.getBlock().setType(Material.AIR);
        }
    }

    @EventHandler
    public void playerDeath(EntityDamageEvent event) {
        if(plugin.getGame().getMode().getModeType() == ModeType.CTF) {
            if (event.getEntity() instanceof Player) {
                Player player = (Player) event.getEntity();
                System.out.println(player.getDisplayName() + " has taken " + event.getDamage() + " points of damage.");
                if(player.getEquipment().getChestplate() != null) {
                    player.getEquipment().getChestplate().setDurability((short) 0);
                }
                if(player.getEquipment().getLeggings() != null) {
                    player.getEquipment().getLeggings().setDurability((short) 0);
                }
                if(player.getEquipment().getBoots() != null) {
                    player.getEquipment().getBoots().setDurability((short) 0);
                }
                if (player.getHealth() - event.getDamage() <= 0) {
                    event.setCancelled(true);
                    if(hasFlag(player)){
                        ItemStack air = player.getEquipment().getItemInMainHand();
                        air.setType(Material.AIR);
                        air.setAmount(1);
                        player.getEquipment().setHelmet(air);
                        respawnFlag(mScoreboardLogic.getTeam(player));
                        Collection<? extends org.bukkit.entity.Player> players = Bukkit.getOnlinePlayers();
                        players.forEach(playerList -> {
                            if(mScoreboardLogic.getTeam(player) == mScoreboardLogic.getTeamOne()) {
                                playerList.sendMessage(ChatColor.BLUE + player.getDisplayName() + ChatColor.RESET + " has dropped the " + ChatColor.RED + "Red Flag!");
                            }
                            if(mScoreboardLogic.getTeam(player) == mScoreboardLogic.getTeamTwo()){
                                playerList.sendMessage(ChatColor.RED + player.getDisplayName() + ChatColor.RESET + " has dropped the " + ChatColor.BLUE + "Blue Flag!");
                            }
                        });
                    }
                    mSpawnLogic.spawnEvent(player);
                    player.playSound(player.getLocation(), Sound.ENTITY_BAT_DEATH, 5, 5);
                    player.setHealth(20);
                }
            }
        }
    }
}
