package me.g0ldensp00n.handlers;

import com.mysql.fabric.xmlrpc.base.Array;
import me.g0ldensp00n.gamelogic.Game;
import me.g0ldensp00n.main.EggSplosion;
import net.minecraft.server.v1_10_R1.NBTTagCompound;
import net.minecraft.server.v1_10_R1.NBTTagList;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;

public class PlayerStoreHandler implements Listener {

    private  Game mGame;

    private long timeMilis;

    private Inventory shopInventoryMain;
    private Inventory shopInventoryCase;
    private Inventory shopInventoryExplosion;

    public PlayerStoreHandler(EggSplosion plugin, Game game){
        this.mGame = game;
        shopInventoryMain = Bukkit.getServer().createInventory(null, 54, "EggSplosion Shop");
        shopInventoryCase = Bukkit.getServer().createInventory(null, 54, "Case");
        shopInventoryExplosion = Bukkit.getServer().createInventory(null, 54, "Explosion");
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void playerShop(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
            if(player.getInventory().getItemInMainHand().getType().equals(Material.EMERALD)){
                if (timeMilis + 50 < System.currentTimeMillis()) {
                    setupShop(player);
                    player.openInventory(shopInventoryMain);
                    timeMilis = System.currentTimeMillis();
                }
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();
            if (event.getInventory().getName().equals("EggSplosion Shop")) {
                event.setCancelled(true);
                if (shopInventoryMain.getItem(event.getSlot()).getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Cases")) {
                    player.openInventory(shopInventoryCase);
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                } else if (shopInventoryMain.getItem(event.getSlot()).getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Explosions")){
                    player.openInventory(shopInventoryExplosion);
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                }
            } else if (event.getInventory().getName().equals("Case")) {
                event.setCancelled(true);
                if (shopInventoryCase.getItem(event.getSlot()).getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Go Back!")) {
                    player.openInventory(shopInventoryMain);
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                } else {
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 0.5f);
                    player.sendMessage(ChatColor.RED + "You don't have enough coins!");
                }
            } else if (event.getInventory().getName().equals("Explosion")) {
                event.setCancelled(true);
                if (shopInventoryExplosion.getItem(event.getSlot()).getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Go Back!")) {
                    player.openInventory(shopInventoryMain);
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                } else if(shopInventoryExplosion.getItem(event.getSlot()).getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Explosion Size One")){
                    player.playSound(player.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 2);
                    player.sendMessage(ChatColor.GREEN + "Equipped " + ChatColor.GOLD + "Explosion Size One");
                }else {
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 0.5f);
                    player.sendMessage(ChatColor.RED + "You don't have enough coins!");

                }
            }
        }
    }

    private void setupShop(Player player){
        //Main Interface
        ItemStack exsplosionSelect = createItem(Material.FIREBALL, 1, 0, ChatColor.GREEN + "Explosions", ChatColor.GRAY + "Select your weapon's explosion size!", false);
        ItemStack caseSelect = createItem(Material.IRON_HOE, 1, 0, ChatColor.GREEN + "Cases", ChatColor.GRAY + "Select your weapon's look!", false);
        ItemStack laserSelect = createItem(Material.INK_SACK, 1, 1, ChatColor.GREEN + "Lasers", ChatColor.GRAY + "Select your explosion color!", false);
        ItemStack muzzleSelect = createItem(Material.WOOD, 1, 0, ChatColor.GREEN + "Muzzles", ChatColor.GRAY + "Select your particle effect!", false);
        ItemStack triggerSelect = createItem(Material.STONE_BUTTON, 1, 0, ChatColor.GREEN + "Triggers", ChatColor.GRAY + "Select your firing speed!", false);

        shopInventoryMain.setItem(29, exsplosionSelect);
        shopInventoryMain.setItem(30, caseSelect);
        shopInventoryMain.setItem(31, laserSelect);
        shopInventoryMain.setItem(32, muzzleSelect);
        shopInventoryMain.setItem(33, triggerSelect);

        //UI
        ItemStack backArrow = createItem(Material.ARROW, 1, 0, ChatColor.GREEN + "Go Back!", "", false);
        ItemStack coinCountEmerald = createItem(Material.EMERALD, 1, 0, ChatColor.GREEN + "Current Coins!", " ", false);

        //Explosion Selection

        ItemStack explosionOne = createItem(Material.FIREBALL, 1, 0, ChatColor.GREEN + "Explosion Size One", ChatColor.GRAY + "Select explosion size!,  ," + ChatColor.RESET + ChatColor.GRAY + "Click to equip!", false);
        ItemStack explosionTwo = createItem(Material.FIREBALL, 2, 0, ChatColor.GREEN + "Explosions Size Two", ChatColor.GRAY + "Select explosion size!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 9000," + ChatColor.GRAY + "Click to purchase!", false);
        ItemStack explosionThree = createItem(Material.FIREBALL, 3, 0, ChatColor.GREEN + "Explosions Size Three", ChatColor.GRAY + "Select explosion size!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 10000," + ChatColor.GRAY + "Click to purchase!", false);
        ItemStack explosionFour = createItem(Material.FIREBALL, 4, 0, ChatColor.GREEN + "Explosions Size Four", ChatColor.GRAY + "Select explosion size!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 200000," + ChatColor.GRAY + "Click to purchase!", false);
        ItemStack explosionFive = createItem(Material.FIREBALL, 5, 0, ChatColor.GREEN + "Explosions Size Five", ChatColor.GRAY + "Select explosion size!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 1000000," + ChatColor.GRAY + "Click to purchase!", false);

        shopInventoryExplosion.setItem(11, explosionOne);
        shopInventoryExplosion.setItem(12, explosionTwo);
        shopInventoryExplosion.setItem(13, explosionThree);
        shopInventoryExplosion.setItem(14, explosionFour);
        shopInventoryExplosion.setItem(15, explosionFive);

            //UI Explosion
        shopInventoryExplosion.setItem(48, backArrow);
        shopInventoryExplosion.setItem(49, coinCountEmerald);

        //Case Selection
        ItemStack woodCase = createItem(Material.WOOD_HOE, 1, 0, ChatColor.GREEN + "Wood Case", ChatColor.GRAY + "Choose an appearance!,  ," + ChatColor.RESET + ChatColor.GRAY + "Click to equip!", false);
        ItemStack stoneCase = createItem(Material.STONE_HOE, 1, 0, ChatColor.GREEN + "Stone Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 1500," + ChatColor.GRAY + "Click to purchase!", false);
        ItemStack ironCase = createItem(Material.IRON_HOE, 1, 0, ChatColor.GREEN + "Iron Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 2500," + ChatColor.GRAY + "Click to purchase!", false);
        ItemStack goldCase = createItem(Material.GOLD_HOE, 1, 0, ChatColor.GREEN + "Gold Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 4000," + ChatColor.GRAY + "Click to purchase!", false);
        ItemStack diamondCase = createItem(Material.DIAMOND_HOE, 1, 0, ChatColor.GREEN + "Diamond Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 7000," + ChatColor.GRAY + "Click to purchase!", false);

        ItemStack woodCaseShiny = createItem(Material.WOOD_HOE, 1, 0, ChatColor.GREEN + "Wood Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 2000," + ChatColor.GRAY + "Click to purchase!", true);
        ItemStack stoneCaseShiny = createItem(Material.STONE_HOE, 1, 0, ChatColor.GREEN + "Stone Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 10000," + ChatColor.GRAY + "Click to purchase!", true);
        ItemStack ironCaseShiny = createItem(Material.IRON_HOE, 1, 0, ChatColor.GREEN + "Iron Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 200000," + ChatColor.GRAY + "Click to purchase!", true);
        ItemStack goldCaseShiny = createItem(Material.GOLD_HOE, 1, 0, ChatColor.GREEN + "Gold Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 14000," + ChatColor.GRAY + "Click to purchase!", true);
        ItemStack diamondCaseShiny = createItem(Material.DIAMOND_HOE, 1, 0, ChatColor.GREEN + "Diamond Case", ChatColor.GRAY + "Choose an appearance!, ," + ChatColor.RESET + ChatColor.GRAY + "Cost:" + ChatColor.GOLD + " 1000000," + ChatColor.GRAY + "Click to purchase!", true);


        shopInventoryCase.setItem(11, woodCase);
        shopInventoryCase.setItem(12, stoneCase);
        shopInventoryCase.setItem(13, ironCase);
        shopInventoryCase.setItem(14, goldCase);
        shopInventoryCase.setItem(15, diamondCase);
        shopInventoryCase.setItem(20, woodCaseShiny);
        shopInventoryCase.setItem(21, stoneCaseShiny);
        shopInventoryCase.setItem(22, ironCaseShiny);
        shopInventoryCase.setItem(23, goldCaseShiny);
        shopInventoryCase.setItem(24, diamondCaseShiny);

            //UI Case
        shopInventoryCase.setItem(48, backArrow);
        shopInventoryCase.setItem(49, coinCountEmerald);

    }

    private ItemStack createItem(Material material, int amount, int shrt, String displayname, String lore, boolean shiny) {
        ItemStack item = new ItemStack(material, amount, (short) shrt);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayname);
        meta.setLore(Arrays.asList(lore.split(",")));
        item.setItemMeta(meta);
        if(shiny){
            item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 10);
        }
        return removeAttributes(item);
    }

    public static ItemStack removeAttributes(ItemStack i){
        if(i == null) {
            return i;
        }
        if(i.getType() == Material.BOOK_AND_QUILL) {
            return i;
        }
        ItemStack item = i.clone();
        net.minecraft.server.v1_10_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag;
        if (!nmsStack.hasTag()){
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        else {
            tag = nmsStack.getTag();
        }
        NBTTagList am = new NBTTagList();
        tag.set("AttributeModifiers", am);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }
}
