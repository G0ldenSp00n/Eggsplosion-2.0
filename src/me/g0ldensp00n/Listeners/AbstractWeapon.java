package me.g0ldensp00n.listeners;

import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.EntityEffect;
import org.bukkit.Material;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractWeapon {
    private final EggSplosion plugin;
    private final List<String> coolDown = new ArrayList<>();

    int delay;
    int velocity;
    Material type;

    private long timeMilis;
    private long timeMilisXP;


    AbstractWeapon(EggSplosion plugin) {
        this.plugin = plugin;
    }

    abstract void hoeType();

    abstract void delayTime();

    abstract void setVelocity();

    public void playerInteract(PlayerInteractEvent event) {
        shootHoe(event);
    }

    private void shootHoe(PlayerInteractEvent event) {
        Player player = event.getPlayer();
//        enabled = hoeHandler.getHoe();
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (player.getEquipment().getItemInMainHand().getType() == type) {
                if (!coolDown.contains(player.getUniqueId().toString())) {
                    Egg egg = player.launchProjectile(Egg.class);
                    egg.setCustomName(type.name());
                    egg.setVelocity(egg.getVelocity().multiply(velocity));
                    coolDown.add(player.getUniqueId().toString());
                    xpLoading(player, delay);

                    //TODO: Remove Scheduler
                    if(timeMilis + (delay * 50) < System.currentTimeMillis()){
                        coolDown.remove(player.getUniqueId().toString());
                        timeMilis = System.currentTimeMillis();
                    }
                }
            }
        }
    }

    private void xpLoading(Player player, long delayLength) {
        new BukkitRunnable() {
            float maxXP = 1.0F;
            float divideXP = maxXP / delayLength;

            @Override
            public void run() {
                if (maxXP > 0) {
                    player.setExp(maxXP);
                    maxXP = maxXP - divideXP;
                } else {
                    player.setExp(0);
                    cancel();
                }
            }
        }.runTaskTimer(this.plugin, 0, 1);
    }
}
