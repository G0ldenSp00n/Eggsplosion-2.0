package me.g0ldensp00n.listeners;

import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class WoodenHoe extends AbstractWeapon implements Listener {

    private Material hoeType = Material.WOOD_HOE;
    private int delayTime = 40;
    private int velocity = 2;

    public WoodenHoe(EggSplosion plugin) {
        super(plugin);
        this.hoeType();
        this.delayTime();
        this.setVelocity();
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void hoeType() {
        super.type = hoeType;
    }

    @Override
    public void setVelocity() {
        super.velocity = velocity;
    }

    @Override
    public void delayTime() {
        super.delay = delayTime;
    }

    @EventHandler
    public void playerInteract(PlayerInteractEvent event) {
        super.playerInteract(event);
    }
}
