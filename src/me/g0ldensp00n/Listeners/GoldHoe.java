package me.g0ldensp00n.listeners;

import me.g0ldensp00n.main.EggSplosion;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class GoldHoe extends AbstractWeapon implements Listener {
    private Material hoeType = Material.GOLD_HOE;
    private int delayTime = 25;
    private int velocity = 3;

    public GoldHoe(EggSplosion plugin) {
        super(plugin);
        this.hoeType();
        this.delayTime();
        this.setVelocity();
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void hoeType() {
        super.type = hoeType;
    }

    @Override
    public void delayTime() {
        super.delay = delayTime;
    }

    @Override
    public void setVelocity() {
        super.velocity = velocity;
    }

    @EventHandler
    public void playerInteract(PlayerInteractEvent event) {
        super.playerInteract(event);
    }
}
